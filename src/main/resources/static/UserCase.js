const userCase = [{
	name: "Create user \"NekoTest\"",
	url: "/user",
	method: "POST",
	header: "admin-auth",
	body: `{
			"id": 3,
			"age": 26,
			"username": "NekoTest",
			"password": "nekos password",
			"ability": "1|3|4|2|6",
			"highestAbility": "",
			"createdTime": "2018-01-01-23:44:32",
			"lastLoginTime": "",
			"reserved": "",
			"extraInfo": null
			}`,
	verify: {
		expectedCode: 200,
	}
},
{
	name: "Create user \"NekoTest\" with wrong request body",
	url: "/user",
	method: "POST",
	header: "admin-auth",
	body: `{-
			"id": 3,
			"age": 26,
			"username": "NekoTest",
			"password": "nekos password",
			"ability": "1|3|4|2|6",
			"highestAbility": "",
			"createdTime": "2018-01-01-23:44:32",
			"lastLoginTime": "",
			"reserved": "",
			"extraInfo": null
			}`,
	verify: {
		expectedCode: 500,
	}
},
{
	name: "Check user \"NekoTest\" exists or not",
	url: "/user",
	method: "GET",
	header: "admin-auth",
	body: "",
	verify: {
		expectedCode: 200,
		expectedFunction: function(result) {
			caseVars.globalVar = null;
			for(let i = 0; i < result.length; i++) {
				if(result[i].username == "NekoTest") {
					caseVars.globalVar = result[i].id;
				}
			}
			if(caseVars.globalVar == null) {
				throw "User \"NekoTest\" not found"
			}
		}
	}
},
{
	name: "Delete user \"NekoTest\"",
	url: "/user/",
	method: "DELETE",
	header: "admin-auth",
	body: "",
	before: function() {
		this.url = "/user/" + caseVars.globalVar;
	},
	verify: {
		expectedCode: 200,
	}
},
{
	name: "Delete user with wrong ID",
	url: "/user/-1",
	method: "DELETE",
	header: "admin-auth",
	body: "",
	verify: {
		expectedCode: 404,
	}
},
{
	name: "Get user with wrong ID",
	url: "/user/-1",
	method: "DELETE",
	header: "admin-auth",
	body: "",
	verify: {
		expectedCode: 404,
	}
},
{
	name: "Unauthorized request",
	url: "/user",
	method: "GET",
	header: "stupid-authorization",
	body: "",
	verify: {
		expectedCode: 401,
	}
},
]