package cumt.innovative.training.project.basketballappointment.utils.utilinterface;

import java.sql.ResultSet;

public interface UtilExecutable {
    public Object execute(ResultSet resultSet, String columnName);
}